# markdown-spellcheck

A Node.js style checker and lint tool for Markdown/CommonMark files.
[markdown-spellcheck](https://www.npmjs.com/package/markdown-spellcheck).

To enable markdown-spellcheck in your project's pipeline add 
`markdown-spellcheck` to the `ENABLE-JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  DISABLE_JOBS: "build, test"
  ENABLE_JOBS: "markdown-spellcheck"
```

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
